package pl.sdacademy.database.daoimpl;

import org.hibernate.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.sdacademy.database.dao.RunMemberDao;
import pl.sdacademy.database.entity.RunMember;
import pl.sdacademy.database.utils.HibernateUtils;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RunMemberDaoImplTest {

    private RunMemberDao runMemberDao = new RunMemberDaoImpl();

    @BeforeEach
    public void clearTable(){
        Session session = HibernateUtils
                .getInstance()
                .getSessionFactory()
                .getCurrentSession();
        session.beginTransaction();
        session.createQuery("delete RunMember")
                .executeUpdate();
        session.getTransaction().commit();
        session.close();
    }
    @Test
    void save() {
        //Given
        RunMember runMember = new RunMember();
        runMember.setName("test przed update");
        runMember.setStartNumber(1);
        //When
        runMemberDao.save(runMember);
        runMember.setName("Nazwa inna");
        runMember.setStartNumber(99);
        runMemberDao.save(runMember);
        RunMember updated = runMemberDao.findById(runMember.getId());
        //THEN
        assertNotNull(updated);
        assertEquals(runMember.getName(), updated.getName());
        assertEquals(runMember.getId(), updated.getId());
    }
    @Test
    void findAll() {
        RunMember runMember1 = new RunMember();
        runMember1.setName("Andrzej");
        runMember1.setStartNumber(1);
        RunMember runMember2 = new RunMember();
        runMember2.setName("Inny Andrzej");
        runMember2.setStartNumber(100);
        runMemberDao.save(runMember1);
        runMemberDao.save(runMember2);
        //THEN
        List<RunMember> list = runMemberDao.findAll();
        assertNotNull(list);
        assertEquals(2, list.size());
    }
    @Test
    void deleteById() {
        //Given
        RunMember runMember = new RunMember();
        runMember.setName("test przed update");
        runMember.setStartNumber(10);
        //WHEN
        runMemberDao.save(runMember);
        RunMember saved = runMemberDao.findById(runMember.getId());
        assertNotNull(saved);
        runMemberDao.deleteById(runMember.getId());
        RunMember deleted = runMemberDao.findById(runMember.getId());
//THEN
        assertNull(deleted);
    }

    @Test
    void findByRegisterDateRange() {
        RunMember member1 = new RunMember();
        member1.setRegisterDate(LocalDate.of(2010, 1, 1));

        RunMember member2 = new RunMember();
        member2.setRegisterDate(LocalDate.of(2015, 1, 1));

        RunMember member3 = new RunMember();
        member3.setRegisterDate(LocalDate.of(2020, 1, 1));

        runMemberDao.save(member1);
        runMemberDao.save(member2);
        runMemberDao.save(member3);

        assertEquals(3, runMemberDao
                .findByRegisterDateRange(
                        LocalDate.of(2009, 1, 1),
                        LocalDate.of(2021, 1, 1)).size());

        assertEquals(1, runMemberDao
                .findByRegisterDateRange(
                        LocalDate.of(2014, 1, 1),
                        LocalDate.of(2016, 1, 1)).size());
    }
}