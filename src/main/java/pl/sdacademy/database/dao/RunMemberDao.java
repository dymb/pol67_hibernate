package pl.sdacademy.database.dao;

import pl.sdacademy.database.entity.RunMember;

import java.time.LocalDate;
import java.util.List;

public interface RunMemberDao {
    void save(RunMember runMember); //C, U
    RunMember findById(Integer id); //R
    List<RunMember> findAll();      //R
    void deleteById(Integer id);    //D

    List<RunMember> findByRegisterDateRange(LocalDate min, LocalDate max);
}
