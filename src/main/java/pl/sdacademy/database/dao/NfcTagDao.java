package pl.sdacademy.database.dao;


import pl.sdacademy.database.entity.NfcTag;

import java.util.List;

public interface NfcTagDao {
    void save(NfcTag nfcTag); //C, U
    NfcTag findById(Integer id); //R
    List<NfcTag> findAll();      //R
    void deleteById(Integer id); //D
}
