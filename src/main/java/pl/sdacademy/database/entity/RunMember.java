package pl.sdacademy.database.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "RUN_MEMBER")
public class RunMember {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(length = 100)
    private String name;
    @Column(name = "start_number")
    private Integer startNumber;
    private Integer age;

    @ManyToOne
    @JoinColumn(name = "run_id")
    private Run run;

    @ManyToMany
    @JoinTable(
            name = "RUN_MEMBER_NFC_TAG",
            joinColumns = { @JoinColumn(name = "id_run_member") },
            inverseJoinColumns = { @JoinColumn(name = "id_nfc_tag") })
    private Set<NfcTag> tagSet = new HashSet<>();

    private LocalDate registerDate;

    public RunMember() {}

    public LocalDate getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(LocalDate registerDate) {
        this.registerDate = registerDate;
    }

    public Set<NfcTag> getTagSet() {
        return tagSet;
    }

    public void setTagSet(Set<NfcTag> tagSet) {
        this.tagSet = tagSet;
    }

    public Run getRun() {
        return run;
    }

    public void setRun(Run run) {
        this.run = run;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStartNumber() {
        return startNumber;
    }

    public void setStartNumber(Integer startNumber) {
        this.startNumber = startNumber;
    }
}
