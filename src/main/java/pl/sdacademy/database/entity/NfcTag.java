package pl.sdacademy.database.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "NFC_TAG")
public class NfcTag {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String serialNumber;

    @ManyToMany(mappedBy = "tagSet")
    private Set<RunMember> runMembers = new HashSet<>();


    public Set<RunMember> getRunMembers() {
        return runMembers;
    }

    public void setRunMembers(Set<RunMember> runMembers) {
        this.runMembers = runMembers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
