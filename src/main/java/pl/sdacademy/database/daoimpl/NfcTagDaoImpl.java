package pl.sdacademy.database.daoimpl;

import org.hibernate.Session;
import pl.sdacademy.database.dao.NfcTagDao;
import pl.sdacademy.database.entity.NfcTag;
import pl.sdacademy.database.utils.HibernateUtils;

import java.util.List;

public class NfcTagDaoImpl implements NfcTagDao {
    public void save(NfcTag nfcTag) {
        Session session = HibernateUtils
                .getInstance()
                .getSessionFactory()
                .getCurrentSession();
        session.beginTransaction();

        session.save(nfcTag);

        session.getTransaction().commit();
        session.close();
    }

    public NfcTag findById(Integer id) {
        Session session = HibernateUtils
                .getInstance()
                .getSessionFactory()
                .getCurrentSession();
        session.beginTransaction();

        NfcTag nfcTag = session
                .createQuery("from NfcTag where id=:id", NfcTag.class)
                .setParameter("id", id)
                .uniqueResultOptional()
                .orElse(null);

        session.getTransaction().commit();
        session.close();

        return nfcTag;
    }

    public List<NfcTag> findAll() {
        Session session = HibernateUtils
                .getInstance()
                .getSessionFactory()
                .getCurrentSession();
        session.beginTransaction();

        List<NfcTag> list = session
                .createQuery("from NfcTag", NfcTag.class)
               .list();

        session.getTransaction().commit();
        session.close();

        return list;
    }

    public void deleteById(Integer id) {
        Session session = HibernateUtils
                .getInstance()
                .getSessionFactory()
                .getCurrentSession();
        session.beginTransaction();

        session
                .createQuery("delete NfcTag where id=:id")
                .setParameter("id", id)
                .executeUpdate();

        session.getTransaction().commit();
        session.close();
    }
}
