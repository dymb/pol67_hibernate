package pl.sdacademy.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.sdacademy.database.dao.NfcTagDao;
import pl.sdacademy.database.dao.RunDao;
import pl.sdacademy.database.dao.RunMemberDao;
import pl.sdacademy.database.daoimpl.NfcTagDaoImpl;
import pl.sdacademy.database.daoimpl.RunDaoImpl;
import pl.sdacademy.database.daoimpl.RunMemberDaoImpl;
import pl.sdacademy.database.entity.NfcTag;
import pl.sdacademy.database.entity.Run;
import pl.sdacademy.database.entity.RunMember;
import pl.sdacademy.database.utils.HibernateUtils;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {

//        oneToManyLoadTest();

        manyToManySaveTest();
        HibernateUtils
                .getInstance()
                .getSessionFactory()
                .close();
    }

    private static void manyToManySaveTest() {
        RunMemberDao runMemberDao = new RunMemberDaoImpl();
        NfcTagDao nfcTagDao = new NfcTagDaoImpl();

        NfcTag tag1 = new NfcTag();
        tag1.setSerialNumber("numer 1");
        nfcTagDao.save(tag1);

        NfcTag tag2 = new NfcTag();
        tag2.setSerialNumber("numer 2");
        nfcTagDao.save(tag2);

        RunMember member1 = new RunMember();
        member1.setName("Wojtek Biegacz");
        member1.getTagSet().add(tag1);
        member1.getTagSet().add(tag2);
        member1.setRegisterDate(LocalDate.of(2010, 1, 1));
        runMemberDao.save(member1);

        RunMember member2 = new RunMember();
        member2.setName("Adam Biegacz");
        member2.getTagSet().add(tag1);
        member2.getTagSet().add(tag2);
        runMemberDao.save(member2);
    }

    private static void oneToManySaveTest() {
        RunDao runDao = new RunDaoImpl();
        RunMemberDao runMemberDao = new RunMemberDaoImpl();

        Run run = new Run();
        run.setName("Wielki wielki bieg");
        run.setMembersLimit(10);
        runDao.save(run);

        for(int i = 0; i < 10; i++) {
            RunMember runMember = new RunMember();
            runMember.setName("Biegacz " + i);
            runMember.setAge(6);
            runMember.setRun(run);
            runMemberDao.save(runMember);
        }
    }

    private static void oneToManyLoadTest() {
        RunDao runDao = new RunDaoImpl();

        Run run = runDao.findById(70);
        System.out.println(run.getName());
        System.out.println(run.getMembers().size());
    }


    public static void saveTest() {
        SessionFactory factory = HibernateUtils
                .getInstance()
                .getSessionFactory();
        Session session = factory.getCurrentSession();
        session.beginTransaction();

        Run run = new Run();
        run.setName("Bieg Hibernate");
        run.setMembersLimit(999);

        session.saveOrUpdate(run);

        session.getTransaction().commit();
        session.close();
    }

    public static void readOneTest() {
        Session session = HibernateUtils
                .getInstance()
                .getSessionFactory()
                .getCurrentSession();
        session.beginTransaction();

        Run run = session
                .createQuery("from Run where id=:idParam", Run.class)
                .setParameter("idParam", 1)
                .uniqueResultOptional()
                .orElse(null);

        session.getTransaction().commit();
        session.close();

        System.out.printf("%d %s %d\n",
                run.getId(),
                run.getName(),
                run.getMembersLimit());
    }

    public static void deleteOne(Integer id) {
        Session session = HibernateUtils
                .getInstance()
                .getSessionFactory()
                .getCurrentSession();
        session.beginTransaction();

        session.createQuery("delete Run where id=:id")
                .setParameter("id", id)
                .executeUpdate();

        session.getTransaction().commit();
        session.close();
    }
}
